package testwork;

import testwork.home.House;

import java.util.Random;

public class Main {
    private static final Random NUMBER_OF_FLOORS = new Random();
    private static final int NUMBER_OF_PASSAGES_BETWEEN_FLOORS_OF_THE_ELEVATOR = 20;
    private static final int MAX_PASSENGERS = 5;

    public static void main(String[] args) {

        House house = new House((NUMBER_OF_FLOORS.nextInt(16) + 5), MAX_PASSENGERS);
        System.out.println("START");
        System.out.println(house);
        house.start(NUMBER_OF_PASSAGES_BETWEEN_FLOORS_OF_THE_ELEVATOR);
    }
}
