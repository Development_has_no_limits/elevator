package testwork;

import testwork.elevator.AbstractElevator;

public class MyElevator extends AbstractElevator {

    public MyElevator(int maxFloor, int maxPassengers) {
        super(maxFloor, maxPassengers);
    }
}
