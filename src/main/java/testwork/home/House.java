package testwork.home;

import testwork.MyElevator;
import testwork.elevator.Elevator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SuppressWarnings("unchecked")
public class House {
    private final static Random random = new Random();
    private final int floors;
    private final Elevator elevator;
    private final List<Integer>[] passengersOnFloor;


    public House(int floors, int maxPassengers) {
        this.floors = floors;
        this.elevator = new MyElevator(floors, maxPassengers);
        this.passengersOnFloor = new List[floors];
        fillRandomPassengers();
    }


    public void start(int amountThatWillPassBetweenFloors) {
        for (int i = 1; i <= amountThatWillPassBetweenFloors; i++) {
            int removedPassengers = elevator.removePassengers();
            if (elevator.isEmptyElevator()) {
                elevator.setDirection(this.getElevatorDirectionByMajorPartOfPeople());
            }

            int addedPassengers = this.addPassengersToElevator();

            if (removedPassengers == 0 && addedPassengers == 0) {
                i--;
            } else {
                createPassengers(removedPassengers);
                this.showInformation(i, removedPassengers, addedPassengers);
            }
            elevator.move();
        }
    }


    private int addPassengersToElevator() {
        elevator.directionController();

        ArrayList<Integer> indexesToDelete = new ArrayList<>();
        for (int i = 0; i < passengersOnFloor[elevator.getCurrentFloor() - 1].size() && elevator.isFullElevator(); i++) {
            if (elevator.isDirection()) {
                if (passengersOnFloor[elevator.getCurrentFloor() - 1].get(i) > elevator.getCurrentFloor()) {
                    indexesToDelete.add(i);
                    elevator.addPassenger(
                            passengersOnFloor[elevator.getCurrentFloor() - 1].get(i));
                }
            } else {
                if (passengersOnFloor[elevator.getCurrentFloor() - 1].get(i) < elevator.getCurrentFloor()) {
                    indexesToDelete.add(i);
                    elevator.addPassenger(
                            passengersOnFloor[elevator.getCurrentFloor() - 1].get(i));
                }
            }
        }

        for (int i = indexesToDelete.size() - 1; i >= 0; i--) {
            passengersOnFloor[elevator.getCurrentFloor() - 1].remove(i);
        }
        return indexesToDelete.size();
    }

    private void fillRandomPassengers() {
        for (int i = 0; i < floors; i++) {
            passengersOnFloor[i] = fillFloor(i + 1);
        }
    }

    private List<Integer> fillFloor(int currentFloor) {
        ArrayList<Integer> floor = new ArrayList<>();
        int passInTheFloor = random.nextInt(11);
        for (int i = 1; i < passInTheFloor; i++) {
            floor.add(createPassenger(currentFloor));
        }
        return floor;
    }

    private int createPassenger(int currentFloor) {
        int passenger = currentFloor;
        while (passenger == currentFloor)
            passenger = random.nextInt(floors) + 1;

        return passenger;
    }

    private void createPassengers(int count) {
        for (int i = 0; i < count; i++) {
            this.passengersOnFloor[elevator.getCurrentFloor() - 1].add(createPassenger(elevator.getCurrentFloor()));
        }
    }

    private boolean getElevatorDirectionByMajorPartOfPeople() {
        if (elevator.getCurrentFloor() == 1) {
            return true;
        } else if (elevator.getCurrentFloor() == floors) {
            return false;
        } else {
            int peoplesWhoWantUp = 0;
            for (int i = 0; i < passengersOnFloor[elevator.getCurrentFloor() - 1].size(); i++)
                if (passengersOnFloor[elevator.getCurrentFloor() - 1].get(i) > elevator.getCurrentFloor())
                    peoplesWhoWantUp++;

            return passengersOnFloor[elevator.getCurrentFloor() - 1].size() - peoplesWhoWantUp < peoplesWhoWantUp;
        }
    }

    private void showInformation(int step, int removedPassengers, int addedPassengers) {
        System.out.println("----------- Step " + step + " -----------");
        System.out.print(this);
        System.out.println("Leave: " + removedPassengers + " Entry: " + addedPassengers + "\n");
    }

    public String toString() {
        StringBuilder result = new StringBuilder();

        for (int i = floors - 1; i >= 0; i--) {
            if (elevator.getCurrentFloor() != i + 1)
                result.append("").append(i + 1).append(" floor: ").append(passengersOnFloor[i].toString()).append("\n");
            else
                result.append("").append(i + 1).append(" floor: ").append(passengersOnFloor[i].toString()).append(" Lift:{").append(elevator).append("}\n");
        }
        return result.toString();
    }
}
