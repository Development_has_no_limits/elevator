package testwork.elevator;

public abstract class AbstractElevator implements Elevator {
    private static int MAX_PASSENGERS;
    private final int[] passengers;
    private final int numberOfFloors;
    private int currentFloor = 1;
    private boolean direction;

    public AbstractElevator(int numberOfFloors, int maxPassenger) {
        AbstractElevator.MAX_PASSENGERS = maxPassenger;
        this.numberOfFloors = numberOfFloors;
        this.passengers = new int[MAX_PASSENGERS];
    }

    public void move() {
        directionController();

        int nextFloor;
        if (isFullElevator()) {
            nextFloor = direction ? currentFloor + 1 : currentFloor - 1;
        } else nextFloor = findFloorWhenElevatorFull();
        currentFloor = nextFloor;
    }

    public boolean isFullElevator() {
        for (int i : passengers) {
            if (i == 0) {
                return true;
            }
        }
        return false;
    }


    public boolean isEmptyElevator() {
        for (int i : passengers) {
            if (i != 0) {
                return false;
            }
        }
        return true;
    }


    public void addPassenger(int passengerFloor) {
        for (int i = 0; i < MAX_PASSENGERS; i++)
            if (passengers[i] == 0) {
                passengers[i] = passengerFloor;
                return;
            }
    }

    public int removePassengers() {
        int removedPassengersCount = 0;
        for (int i = 0; i < MAX_PASSENGERS; i++)
            if (passengers[i] == currentFloor) {
                passengers[i] = 0;
                removedPassengersCount++;
            }
        return removedPassengersCount;
    }


    private int findFloorWhenElevatorFull() {
        int result;
        if (direction) {
            int min = numberOfFloors + 1;
            for (int i : passengers)
                if (i != 0 && i < min) {
                    min = i;
                }
            result = (min != numberOfFloors + 1) ? min : 0;

        } else {
            int max = 0;
            for (int i : passengers)
                if (i > max) max = i;
            result = max;
        }
        if (result == 0) {
            throw new IllegalStateException("Method can`t find next floor!");
        }
        return result;
    }


    public boolean isDirection() {
        return direction;
    }

    public void setDirection(boolean direction) {
        this.direction = direction;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }


    public void directionController() {
        if (currentFloor == 1) {
            direction = true;
        } else if (currentFloor == numberOfFloors) {
            direction = false;
        }
    }

    public String toString() {
        StringBuilder res = new StringBuilder();
        for (int passenger : passengers) {
            if (passenger != 0) {
                res.append(passenger).append(" ");
            }
        }
        if (res.length() > 0) {
            res.deleteCharAt(res.length() - 1);
        }
        return res.toString();
    }
}
