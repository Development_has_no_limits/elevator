package testwork.elevator;

public interface Elevator {

    void move();

    boolean isEmptyElevator();

    boolean isFullElevator();

    void directionController();

    boolean isDirection();

    int getCurrentFloor();

    void addPassenger(int passengerFloor);

    int removePassengers();

    void setDirection(boolean direction);
}
